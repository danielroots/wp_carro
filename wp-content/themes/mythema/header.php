<!DOCTYPE html>
<html>
<head>
	<title><?php wp_title();?></title>
	<meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>

    <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css">
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/assets/css/jquery.fancybox.min.css"/>
	<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.fancybox.min.js"></script>
	
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	


</head>



<body>



<!--NAVBAR-->
<nav class="navbar fixed-top navbar-dark bg-dark">
  		<a class="navbar-brand" href="<?php echo site_url(); ?>" title="<?php bloginfo('name'); ?>">logo</a>

		  <ul class="nav justify-content-end">
		  <li class="nav-item">
		    <a class="nav-link active" href="<?php echo site_url(); ?>">INICIO</a>
		  </li>

		  <?php 
		  $args = array('post_type'=> 'page', 'pagename' => 'contato'); 
		  $contato = $args;
		  ?>
		  
		  <li class="nav-item">
		    <a class="nav-link" href="<?php  echo $contato['pagename']; ?>">CONTATO</a>
		  </li>

  		  <?php 
		  $args = array('post_type'=> 'page', 'pagename' => 'sobre'); 
		  $sobre = $args;
	
		  ?>
		   <li class="nav-item">
		    <a class="nav-link" href="<?php  echo $sobre['pagename']; ?>">SOBRE</a>
		  </li>
		 
		
		</ul>
</nav>
<!--NAVBAR-->


		

	  <?php if((is_single()) || (is_page()) ): ?>

	 
	  

	  	<!-- HEADER-->
<div class="container-fluid bg-header" id="bg-header">

	<div class="container-fluid" id="bg" style="background: url('<?php the_post_thumbnail_url('full'); ?>') top no-repeat fixed;">
	</div>
	
			<div class="col-md-12 text-center">
			<h1><?php the_title(); ?></h1>	
			</div>
	

</div>
	

<?php else: ?>
<!-- HEADER2-->


<!-- HEADER1-->

<div class="container-fluid" id="index-header">
	<div class="row">
		<div class="col-md-12 text-center">
		<h1><?php bloginfo('name'); ?></h1>	

			<div class="container form-header">

				<?php if(is_category() || is_search()):?>


				
				<?php get_template_part( 'advanced', 'searchform' ); ?>
						
			<?php endif; ?>








			</div>


		</div>
	</div>

</div>
<!-- HEADER-->
<?php endif; ?>





