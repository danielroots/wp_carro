<?php get_header();?>


<?php 

if(isset($_GET['marca']) && !empty($_GET['marca'])){
	$_marca = $_GET['marca'];
}
if(isset($_GET['modelo']) && !empty($_GET['modelo'])){
	$_modelo = $_GET['modelo'];
}
if(isset($_GET['preco']) && !empty($_GET['preco'])){
	$_preco = $_GET['preco'];
}


?>


<!-- Conteudo -->
<div class="container-fluid" style="margin-top: 15px;">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

			


<!-- SEarc acima -->
				
				<?php if(have_posts()): while(have_posts()): the_post() ?>



							<?php $ano = get_field('ano');
							$km =  get_field('km');
							$marca = get_field('marca');?>

					<div class="col-md-3">
							<div class="img-thumbnail text-center">
								<a href="<?php the_permalink(); ?>">
								
									<img  class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="carro de repasse" style="width: 300px; height: 200px;">
								</a>
								
								<div class="title">
									<span><?php echo short_text(get_the_title_rss(), 37);?></span>
								</div>
								<div class="desc"><br>
									<span hidden> <?php echo $marca; ?></span>
									<span>Mod/Ano: <?php echo $ano; ?></span><br>
									<span>KM: <?php echo $km; ?></span>
								</div>

							</div>
						</div>


					<?php endwhile; ?>
					<?php endif; ?>



			</div>
		</div>
	</div>
</div>
<!-- Conteudo -->

<?php get_footer();?>