<?php

//habilitando imagens destacadas
add_theme_support('post-thumbnails');
add_image_size('thumb-custom', 270, 200, true);	



//add_action('init', 'short_text');

function short_text($text,$tamanho){

	if(strlen($text) > $tamanho){

		$text = substr($text, 0, strrpos(substr($text, 0, $tamanho), ' ' )) . "...";

		return $text;

	}else{
		return $text;
	}

}

function category_link($nome_cat){

	$id = get_cat_ID($nome_cat);

	return get_category_link($id);

}


function get_field_id_post($campo){


	for($i=0; $i<count(get_posts()); $i++){

		$id_post = get_posts()[$i]->ID . "<br>";



		$field = get_field($campo, intval($id_post) );


		echo "<option value=". $field .">" . $field . "</option>";

	}



}

/**
 * Join posts and postmeta tables
 *
 *///http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );


/**
 * Modify the search query with posts_where
 *
 *///http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 

function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 *///http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );



?>