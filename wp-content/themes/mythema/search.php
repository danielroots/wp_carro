<?php get_header(); ?>
<?php


if($_GET['marca'] && !empty($_GET['marca'])){
	$_marca = $_GET['marca'];
}

if($_GET['modelo'] && !empty($_GET['modelo'])){
	$_modelo = $_GET['modelo'];
}


if($_GET['preco'] && !empty($_GET['preco'])){
	$_preco = $_GET['preco'];
}


?>

<?php 
/*
 INNER JOIN $wpdb->postmeta.post_id

    WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 

    AND $wpdb->postmeta.meta_key = 'marca' 

    AND $wpdb->postmeta.meta_value = '$_marca' 
    AND $wpdb->posts.post_status = 'publish' 
    AND $wpdb->posts.post_type = 'post'
    AND $wpdb->posts.post_date < NOW()
    ORDER BY $wpdb->posts.post_date DESC

    ////////////



SELECT      key3.post_id
	FROM        $wpdb->postmeta key3
	INNER JOIN  $wpdb->postmeta key1 
	            ON key1.post_id = key3.post_id
	            AND key1.meta_key = %s 
	INNER JOIN  $wpdb->postmeta key2
	            ON key2.post_id = key3.post_id
	            AND key2.meta_key = %s
	WHERE       key3.meta_key = %s 
	            AND key3.meta_value = %s
	ORDER BY    key1.meta_value, key2.meta_value

*/
$custom_query = " SELECT $wpdb->posts.*  
    FROM $wpdb->posts 
    INNER JOIN $wpdb->postmeta as key1 ON $wpdb->posts.ID = key1.post_id 
    AND key1.meta_key = 'marca' 
    AND key1.meta_value = '$_marca'
    AND $wpdb->posts.post_status = 'publish'
    AND $wpdb->posts.post_type = 'post'
    AND $wpdb->posts.post_date < NOW()
    INNER JOIN $wpdb->postmeta as key2 ON $wpdb->posts.ID = key2.post_id 
    AND key2.meta_key = 'modelo' 
    AND key2.meta_value = 'Toyota  Corolla ALTIS 2.0 Flex 16V Aut.'
    AND $wpdb->posts.post_status = 'publish'
    AND $wpdb->posts.post_type = 'post'
    AND $wpdb->posts.post_date < NOW()
    INNER JOIN $wpdb->postmeta as key3 ON $wpdb->posts.ID = key3.post_id 
    AND key3.meta_key = 'preco' 
    AND key3.meta_value >= 10.000
    AND $wpdb->posts.post_status = 'publish'
    AND $wpdb->posts.post_type = 'post'
    AND $wpdb->posts.post_date < NOW()
    ORDER BY $wpdb->posts.post_date DESC
    
   ";

$pageposts = $wpdb->get_results($custom_query, OBJECT);



?>

<div class="container-fluid" style="margin-top: 15px;">
	<div class="row">
		<div class="col-md-12">
			<div class="row">


				<?php if ($pageposts): ?>
				 <?php global $post; ?>
				 <?php foreach ($pageposts as $post): ?>
				 <?php setup_postdata($post); ?>

				 <?php $ano = get_field('ano');
							$km =  get_field('km');
							$marca = get_field('marca');?>

					<div class="col-md-3">
							<div class="img-thumbnail text-center">
								<a href="<?php the_permalink(); ?>">
								
									<img  class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="carro de repasse" style="width: 300px; height: 200px;">
								</a>
								
								<div class="title">
									<span><?php echo short_text(get_the_title_rss(), 37);?></span>
								</div>
								<div class="desc"><br>
									<span hidden> <?php echo $marca; ?></span>
									<span>Mod/Ano: <?php echo $ano; ?></span><br>
									<span>KM: <?php echo $km; ?></span>
								</div>

							</div>
						</div>

				 
				


				 <?php endforeach; ?>
				 <?php else : ?>
				 	<div class="col-md-12 text-center">
				 		  <h2 class="center">Nenhum carro foi encontrado!</h2>
				 	</div>
				  
				   
				 <?php endif; ?>



			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>