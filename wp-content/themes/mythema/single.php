<?php get_header();?>
<?php  //error_reporting(0); ?>
<!-- Conteudo -->
<div class="container-fluid" style="margin-top: 15px;">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="col-md-6 infocar">
					<div class="row">
						<div class="col-md-12 infoheader">
							<h5>Galeria</h5>
						</div>	
					</div>

					

					<div class="col-md-12 text-center" style="margin-top: 5px;">
						<div class="row">

						<?php $image = easy_image_gallery_get_image_ids(); 
						$cont = 0;


						?>

						<?php if($image): 
							foreach($image as $img_id): 
								$image_full = wp_get_attachment_image_src($img_id, '');
								$img = wp_get_attachment_image_src($img_id, '');

								

								if($cont <= 8):
						 ?>

									 	<div class="col-md-4" style="padding: 5px !important;">
											<a href="<?php echo $image_full[0] ?>"  data-fancybox="images">
												<img class="img-fluid galeria-img" alt="<?php echo get_field('modelo'); ?>" src="<?php echo $img[0] ?>"/>
											</a>
										</div>
								<?php endif; ?>

								

								<?php $cont++; ?>
							<?php endforeach; ?>

							<?php else: ?>
								<div class="col-md-12" style="padding: 5px !important;">
								<h5>Nenhuma foto foi encontrada!</h5>
								</div>
						<?php endif; ?>


							
							
						</div>
					</div>


				</div>

				<div class="col-md-3 infocar">
						<div class="row">
							<div class="col-md-12 infoheader" style="margin-bottom:15px;">
								<h5>Detalhes do Veículo</h5>
							</div>
						</div>
						<div class="col-md-12">
							<form role="form" style="float:left;">
								<div class="form-group">
									<label>Modelo</label>
									<p><?php echo get_field('modelo'); ?></p>
								</div>
								<div class="form-group">
									<label>Marca</label>
									<p><?php echo get_field('marca'); ?></p>
								</div>
								<div class="form-group">
									<label>Ano</label>
									<p><?php echo get_field('ano'); ?></p>
								</div>
								<div class="form-group">
									<label>Cor</label>
									<p><?php echo get_field('cor'); ?></p>
								</div>
								<div class="form-group">
									<label>Km</label>
									<p><?php echo get_field('km'); ?></p>
								</div>
								<div class="form-group">
									<label>Final Placa</label>
									<p>***<?php echo get_field('final_placa'); ?></p>
								</div>
								<div class="form-group">
									<label>Preço</label>
									<span class="price"><p>R$ <?php echo get_field('preco'); ?></p></span>
								</div>
							</form>
						</div>
					
				</div>

				<div class="col-md-3 infocar">
					<div class="row">
						<div class="col-md-12 infoheader" style="margin-bottom:15px;">
							<h5>Opcionais / Observações</h5>
						</div>
					</div>
					<div class="col-md-12 text-left">
						<h6><b>Opcionais</b></h6>
						<ul style="padding-left: 19px;">
					
							<?php if(get_field('opcionais') != null): 
									foreach(get_field('opcionais') as $opcoes):?>
										<li><?php echo $opcoes ?></li>
							
							
									<?php endforeach; ?>
									<?php else: ?>
										<li>Nenhum opcional foi adicionado</li>
							<?php endif ?>
						</ul>

					</div>
					<div class="col-md-12 text-left">
						<h6><b>Observações</b></h6>
						<?php if(get_field('observacoes')): ?>
						<p><?php echo get_field('observacoes'); ?></p>
						<?php else: ?>
							<p>Nenhuma observação foi adicionada</p>
						<?php endif; ?>
					</div>
					<div class="col-md-12 text-right" style="margin-top:105px;">
						<button class="btn btn-dark">Enviar Proposta</button>
					</div>
				</div>

				
			</div>
		</div>
	</div>
</div>
<!-- Conteudo -->


<?php get_footer();?>