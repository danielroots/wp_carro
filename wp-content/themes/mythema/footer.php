

<?php wp_footer();?>
<!-- footer -->
<div class="container-fluid" style="margin-top: 15px; background: #999;">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="footer" style="height: 150px; padding: 50px 0 0 0;">
				<p>© Repasse Rio</p>
			</div>
		</div>
	</div>
</div>
<!-- footer -->
<script type="text/javascript">

	var modelos = $('select[name="modelo"] option');

$('select[name="marca"]').on('change', function () {
    var marcas = this.value;
    var novoSelect = modelos.filter(function () {
        return $(this).data('marca') == marcas;
    });
    $('select[name="modelo"]').html(novoSelect);
});

</script>
<script type="text/javascript" src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/js/bootstrap.min.js"></script>
</body>
</html>