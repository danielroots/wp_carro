<?php get_header();?>


<!-- Conteudo -->
<div class="container-fluid" style="margin-top: 15px;">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<?php 
				$args = array('post_type'=> 'post', 'showposts' => 8); 
				$my_posts = get_posts($args);


				

				?>
				<?php if(have_posts()): ?>
				<?php $contador = 0;?>	
				<?php if($my_posts): 
					foreach($my_posts as $post): 
							setup_postdata($post);

							$ano = get_field('ano');
							$km =  get_field('km');
							$marca = get_field('marca');

					

						?>

						<div class="col-md-3">
							<div class="img-thumbnail text-center">
								<a href="<?php the_permalink(); ?>">
								
									<img  class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="carro de repasse" style="width: 300px; height: 200px;">
								</a>
								
								<div class="title">
									<span><?php echo short_text(get_the_title_rss(), 37);?></span>
								</div>
								<div class="desc"><br>
									<span hidden> <?php echo $marca; ?></span>
									<span>Mod/Ano: <?php echo $ano; ?></span><br>
									<span>KM: <?php echo $km; ?></span>
								</div>

							</div>
						</div>

				<?php endforeach; ?>
				<?php endif; ?>

				<?php if(count($my_posts) == 8): ?>

				

								<div class="col-md-12 text-center" style="margin-top:50px;">
									<a class="btn btn-danger" href="<?php echo category_link('carros') ?>">Ver estoque completo</a>
								</div>
							<?php endif; ?>


			
					
				<?php endif; ?>
				

				
			</div>
		</div>
	</div>
</div>
<!-- Conteudo -->

<?php get_footer();?>


<?php 
// the_permanlink() //  pega link baseado no array post_type
//bloginfo( 'template_directory' ); // pega caminhos  diretorio
//the_excerpt() pega resumo baseado no post
// the_title(); 
//the_post_thumbnail();

?>