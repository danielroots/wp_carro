<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wordpress' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Kz7bJJ55)>S yB 8m}pq|b/`m^F:lt}4A8Bv=+.VhojMJf2 r,.*H(Ei9(t ~Ni{' );
define( 'SECURE_AUTH_KEY',  'T-SvslL_rbi/+`O|4P.Vt(HEYz`Wkxwz=KLc=Do?,l%~?5cq*[h)8ZA)uw[KlW-]' );
define( 'LOGGED_IN_KEY',    'YWP[!5*w~Fx^}(=L:ZieK/*llyp[2r6oKA4<~_Mg(JS#g/Gsp|.z%WVb^RVlu6i9' );
define( 'NONCE_KEY',        'yWkJ2R|pWyxcHurn#?vkfKi@4jlxj<-v?1G=2nN[7Alc#[FxF:4$DD|15Y!;>eqk' );
define( 'AUTH_SALT',        'f&_M<^4O7bO-}t15|.fk27KYP0]u45H]zot,COX yBsk6Qd?zPhe}lJ<Lenh%L%`' );
define( 'SECURE_AUTH_SALT', 'l6k9!vWyX9hcBMD8pF4k*@)T%l?0oy<~{C|P-qNo2dX;sv?x5IHro*;iua*EqWt[' );
define( 'LOGGED_IN_SALT',   'B3%`ep-We, <`B+Qo*,!6f4j0KKU7yLWQ;%g|2F,)9w@j%HBOah. =vZrzf}dc|&' );
define( 'NONCE_SALT',       '*sUX9xSsp^?~roNeS}|v19X.vLepR%B*Iox,~(]nrB#SWvl;@r?wcRiuULEB@5W9' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
